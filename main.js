const sendButton = document.querySelector('.button')
const getResultButton = document.querySelector('.calcular-media')
let textArea = document.querySelector('#notas')
let grade = document.querySelector('.nota')
let media = document.querySelector('.media')
let contador = 0
sendButton.addEventListener('click', () => {
  gradeFormat = grade.value.replace(',', '.')
  if (gradeFormat == '') {
    alert('Por favor, insira uma nota')
    return
  } else if (isNaN(gradeFormat)) {
    alert('A nota digitada é inválida, por favor insira uma nota válida')
    return
  } else {
    let nota = parseFloat(gradeFormat)
    if (nota > 10 || nota < 0) {
      alert('A nota digitada é inválida, por favor, insira uma nota válida.')
      return
    } else {
      contador += 1
      textArea.value += 'A nota ' + contador + ' foi ' + nota.toFixed(2) + '\n'
    }
  }
})

getResultButton.addEventListener('click', () => {
  if (textArea.value != '') {
    //array = textArea.value.match(/(\d.?.\d\d)/g) Usando regex para obter somente a nota
    gettingEachAddition = textArea.value.split('\n')
    gettingEachAddition.pop() // eliminando \n no fim da array

    let sum = 0
    gettingEachAddition.forEach(n => {
      let divider = n.split(' ') // dividindo cada frase por espaço em branco
      sum += parseFloat(divider[4])
    })
    mediaNota = sum / gettingEachAddition.length

    media.innerText = mediaNota.toFixed(2)
  } else {
    alert('Por favor, insira uma nota.')
    return
  }
})
